'use strict';
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const mysql = require('mysql');
const fs = require('fs');

const APP_CONFIG = JSON.parse(require('fs').readFileSync('./config.json').toString());
const mysqlConnection = mysql.createConnection(
    APP_CONFIG.mysql
);

//#region Core Modules
const MySQLService = require('./modules/mysql/mysql.service')(mysqlConnection);
const Logger = require('./modules/logger/logger')(MySQLService);

const CommandProcessor = require('./modules/command-processor/command.processor')(fs, MySQLService, Logger);
const SocketHandler = require('./modules/socket-handler/socket.handler.js')(CommandProcessor);
//#endregion

app.get('/', function (req, res) {
    res.send('server is running');
});

io.on('connection', socket => {
    Logger.log('[Socket] New connection');
    
    socket.onAny((event, ...args) => {
        SocketHandler.handle(event, args, socket);
    });
});

http.listen(APP_CONFIG.app.defaultPort, async () => {
    if (mysqlConnection) {
        try {
            await MySQLService.initDB()
            await Logger.initialize();
            await CommandProcessor.initialize();
        } catch (e) {
            console.log(e);
        }
    }
});