module.exports = function ($fs, $MySQLService, $Logger) {
    return {
        preLoadedModules: [],

        initialize: function () {
            return new Promise((resolve, reject) => {
                let path = __dirname + '/../../domain/commands';
                let modules = []
                $fs.readdir(path, (err, subdirs) => {
                    if (err) throw reject(err);
                    let commandsCounter = 0;

                    subdirs.forEach((dir, index) => {
                        let structure = {};
                        let subpath = path + '/' + dir;
                        let files = $fs.readdirSync(subpath);

                        files.forEach(e => {
                            commandsCounter++;
                            let moduleName = e.replace('.js', '');
                            structure[moduleName] = {};
                            let moduleLocation = subpath + '/' + e;
                            structure[moduleName]['location'] = moduleLocation;
                            let content = $fs.readFileSync(moduleLocation).toString();
                            structure[moduleName]['content'] = content;

                            let _module = eval(content);
                            structure[moduleName]['caller'] = _module;
                        })

                        modules.push(structure);

                        if (index == subdirs.length - 1) {
                            if (!$fs.existsSync(__dirname + '/structure')) {
                                try{
                                    $fs.mkdirSync(__dirname + '/structure');
                                }catch(e) {
                                    $Logger.log('[Command Processor] Error in creation of structure dir');
                                }
                            }

                            $fs.writeFileSync(__dirname + '/structure/commands.structure.json', JSON.stringify(modules));
                            if (commandsCounter > 0) {
                                $Logger.log('[Command Processor] Structure initialized ', commandsCounter, ' modules found');
                            }else{
                                $Logger.log('[Command Processor] Structure initialized, but 0 modules found');
                            }

                            this.preLoadedModules = modules;
                            resolve(modules);
                        }
                    });
                });
            });
        },

        handle: function (eventName, eventPayload, socket) {
            let _module = this.preLoadedModules.filter(pModule => {
                return pModule[eventName] != null
            })[0];

            if (_module != undefined || _module != null) {
                _module[eventName].caller(eventPayload, $MySQLService, socket);
            }else{
                return null;
            }
        }
    }
}