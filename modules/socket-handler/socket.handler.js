module.exports = function($CommandProcessor) {
    return {
        handle: function(eventName, args, socket) {
            $CommandProcessor.handle(eventName, args, socket)
        }
    }
}