module.exports = function ($mySQLService) {
    return {
        initialize: function () {
            return new Promise((resolve, reject) => {
                $mySQLService.query(`
                    CREATE TABLE IF NOT EXISTS logger(
                        log_id int primary key auto_increment,
                        log_data text,
                        log_datetime datetime default now()
                    );
                `).then(result => { console.log('[Logger] Initialized'); resolve(result) }, err => reject(err));
            });
        },

        log: function (text, arguments) {
            $mySQLService.query(`
                INSERT INTO logger(
                    log_data
                ) VALUES (?);
            `, [text]).then(_ => {
                if (arguments) {
                    console.log(text, arguments);
                }else{
                    console.log(text);
                }
            });
        }

    }
}