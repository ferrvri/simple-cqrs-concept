module.exports = function ($mysqlConnection) {
    return {

        initDB: function() {
            return new Promise((resolve, reject) => {
                $mysqlConnection.query(`
                    CREATE DATABASE IF NOT EXISTS takki;
                `, (err, result, _) => {
                    if (err) reject(err);
                    console.log('[MySQL] Initialized.');
                    resolve(result);
                })
            });
        },

        query: function (query, params) {
            return new Promise((resolve, reject) => {
                $mysqlConnection.query(query, params, (err, result, _) => {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        }
    }
}